call plug#begin('~/.vim/plug_plugins')
Plug 'othree/xml.vim'
Plug 'morhetz/gruvbox'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'https://github.com/LaTeX-Box-Team/LaTeX-Box'
Plug 'https://github.com/kergoth/vim-bitbake.git'
Plug 'https://github.com/JamshedVesuna/vim-markdown-preview'
Plug 'KabbAmine/zeavim.vim'
Plug 'sirtaj/vim-openscad'
call plug#end()

filetype plugin indent on

set smartindent

set number
set relativenumber
set so=5

set nocompatible
set background=dark
colorscheme gruvbox
syntax on
hi Normal ctermbg=none
set colorcolumn=80

" sane splitting
set splitright
set splitbelow

" mouse support
set mouse=a

" Spelling
hi SpellBad cterm=underline

" markdown
let vim_markdown_preview_github=1
let vim_markdown_preview_browser='firefox'
let vim_markdown_preview_use_xdg_open=1

if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
endif

inoremap <F5> <Esc>:make<Enter>i
nnoremap <F5> :make<Enter>

set wrap
set linebreak
