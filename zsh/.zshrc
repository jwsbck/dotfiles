
#If not running interactively, don't do anything
[[ $- != *i* ]] && return

#environment variables begin
export VISUAL="vim"
export EDITOR=$VISUAL
export PAGER="less"
export SHELL="/bin/zsh"
export TERM="xterm-256color"
#environment variables end

#dockertex
export DOCKERTEX_DEFAULT_TAG="texlive2021"
export DOCKERTEX_TEXMFHOME="$HOME/texmf"

# powerlevel9k - PROMT configuration
#POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir vcs)
#POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status time)

#POWERLEVEL9K_COLOR_SCHEME='dark'
#POWERLEVEL9K_PROMPT_ON_NEWLINE=false
#POWERLEVEL9K_SHORTEN_DIR_LENGTH=2
#POWERLEVEL9K_SHORTEN_STRATEGY='truncate_from_right'
#POWERLEVEL9K_SHORTEN_DELIMITER=''

#zplug stuff begin
source ~/.zplug/init.zsh

zplug "zsh-users/zsh-completions"
zplug "zsh-users/zsh-syntax-highlighting"
zplug "arzzen/calc.plugin.zsh", use:calc.plugin.zsh
zplug "plugins/git", from:oh-my-zsh
zplug "raabf/gitmoji-fuzzy-hook", from:gitlab
zplug "raabf/dockertex", \
	from:gitlab, \
	hook-build:"./posthook.sh --menu-tag latest --menu-volume /media/ext/=/home/ext/"
zplug load
#zplug stuff end

#alias section begin
# ls aliases
# if have exa
if hash exa 2>/dev/null; then
	alias exa='exa -g --git'
	alias l='exa'
	alias la='exa -a'
	alias ll='exa -l'
	alias lla='exa  -lha'
	alias lr='exa -R'                    # recursive ls
# if only have plain ls
else
	alias ls='ls --color=auto'
	alias l='ls'
	alias la='ls -A'
	alias ll='ls -lh'
	alias lla='ls -lha'
	alias lr='ls -R'                    # recursive ls
fi
# if end

alias t='tig'
alias watch='watch -c'
alias vim=$EDITOR
#alias section end

#autocompletion begin
zstyle ':completion::complete:*' use-cache=1
zstyle ':completion:*' auto-description '%d'
zstyle ':completion:*' completer _expand _complete _correct _ignored
zstyle ':completion:*' completions 1
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' file-sort modification
zstyle ':completion:*' format "%{$fg[yellow]%}%d%{$reset_color%}"
zstyle ':completion:*' glob 1
zstyle ':completion:*' group-name ''
zstyle ':completion:*' insert-unambiguous true
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*' '' ''
zstyle ':completion:*' menu select=1
zstyle ':completion:*' original true
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' squeeze-slashes true
zstyle ':completion:*' substitute 1
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' verbose true
zstyle ':compinstall' filename '/home/johannes/.zshrc'

#init the zsh completion
autoload -Uz compinit
compinit

#init the bash compatibility completion
autoload -Uz bashcompinit
bashcompinit

#manpages
zstyle ':completion:*:manuals'    separate-sections true
zstyle ':completion:*:manuals.*'  insert-sections   true
zstyle ':completion:*:man:*'      menu yes select

#processes
zstyle ':completion:*:processes'  command 'ps -au$USER'
zstyle ':completion:*:processes-names' command 'ps c -u ${USER} -o command | uniq'

#urls
zstyle ':completion:*:urls' local 'www' 'public_html' '/srv/http'
#autocompletion end

#command history begin
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=30000
setopt append_history share_history extended_history histverify histignorespace histignoredups
#command history end

#cd magic start
#complete cd
setopt auto_cd

alias ...='cd ../../'
alias ....='cd ../../../'
alias .....='cd ../../../../'
alias ......='cd ../../../../../'
#cd magic end

#key bindings: setting up desired hostory search functions
autoload -U history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end

#key bindings: vim style
bindkey -v

bindkey  "^[[A"        history-beginning-search-backward-end #up-line-or-history
bindkey  "^[[B"      history-beginning-search-forward-end #down-line-or-history

#vcs info start
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git svn
zstyle ':vcs_info:*' max-exports 4

zstyle ':vcs_info:*'            check-for-changes true
zstyle ':vcs_info:*'            get-revision    true
zstyle ':vcs_info:*'            stagedstr       "● "
zstyle ':vcs_info:*'            unstagedstr     "# "
zstyle ':vcs_info:(svn|hg):*'   branchformat    "%b:%r"

#%a=action %b=branch %c=stagedstr %u=unstagedstr %i=revision
#%R=basedir %r=reponame %S=subfolder %s=vcsname
zstyle ':vcs_info:*'            formats         "[%r/%b]"       "%c%u"
zstyle ':vcs_info:*'            actionformats   "[%r/%b =>%a]"  "%c%u"
#vcs info end

#theme
imgc="95"
confc="91"
arc="33"
compc="92"
rawc="47;34"
export LS_COLORS="rs=00:no=00:di=01;36:ln=01;04;33:mh=04:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;35;01:or=01;09;31;40:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42;05:st=37;44:ex=32:*.xz=33:*.jpg=$imgc:*.png=$imgc:*.bmp=$imgc:*.gif=$imgc:*.cfg=$confc:*.ini=$confc:*.conf=$confc:*.cnf=$confc:*.pref=$confc:*rc=$confc:*.tar=$arc:*.zip=$arc:*.xz=$compc:*.gz=$compc:*.bz=$compc:*.lzma=$compc:*.gpg=44;93:*.img=$rawc:*.dat=$rawc:*core=31;04:*.bak=32"

#prompt configuration start
DONTSETRPROMPT=1
setopt prompt_subst

precmd () {
	vcs_info
	#window title:
	print -Pn "\e]0;%n@%M: %~\a"
	psvar[1]=""
	psvar[2]="$vcs_info_msg_0_"
	psvar[3]="$vcs_info_msg_1_"
	psvar[4]="$vcs_info_msg_2_"
	#test $vcs_info_msg_1_ && psvar[4]="$vcs_info_msg_1_" || psvar[4]=`pwd`
}

bright_red=#fb4934

PROMPT="%B%F{green}%n%F{cyan}@%F{blue}%m %F{red}%~ %F{yellow}%1v%2v%f%b "
RPROMPT="%3v%4v%f[%F{yellow}%?%f]%1v%F{blue}:%F{red}%l%F{green} %*%f"
#prompt configuration end

#convenience functions
dec2hex() {
	python3 -c "print(hex($1))"
}

alias pwndbg='gdb -x /usr/share/pwndbg/gdbinit.py'
