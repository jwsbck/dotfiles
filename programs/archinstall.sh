#!/bin/bash

# https://stackoverflow.com/a/30182532
choices=( 'step1' 'step2' )

echo "Device to install to:"
read INSTALL_DEVICE

select choice in "${choices[@]}"; do

  [[ -n $choice ]] || { echo "Invalid choice." >&2; continue; }

  case $choice in
    step1)

# create partition layout
parted -s "$INSTALL_DEVICE" "mklabel gpt"
parted -s "$INSTALL_DEVICE" "mkpart primary 1MiB 513MiB"
parted -s "$INSTALL_DEVICE" "set 1 esp on"
parted -s "$INSTALL_DEVICE" "mkpart primary 513MiB 100%"

# create luks1 container
cryptsetup --batch-mode luksFormat --type=luks1 "$INSTALL_DEVICE""2"
cryptsetup --batch-mode luksOpen "$INSTALL_DEVICE""2" cryptdata

# create file systems for EFI and root partition
mkfs.fat -F32 "$INSTALL_DEVICE""1"
mkfs.btrfs /dev/mapper/cryptdata

# create btrfs subvolumes
mount /dev/mapper/cryptdata /mnt
btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@swap
btrfs subvolume create /mnt/@snapshots
umount /mnt

mount -o ssd,space_cache=v2,commit=120,compress=zstd,subvol=@ /dev/mapper/cryptdata /mnt

mkdir /mnt/home
mkdir /mnt/swap
mkdir /mnt/snapshots
mount -o ssd,space_cache,commit=120,compress=zstd,subvol=@home /dev/mapper/cryptdata /mnt/home
mount -o ssd,space_cache,commit=120,compress=zstd,subvol=@swap /dev/mapper/cryptdata /mnt/swap
mount -o ssd,space_cache,commit=120,compress=zstd,subvol=@snapshots /dev/mapper/cryptdata /mnt/snapshots

swapfile=/mnt/swap/swapfile
truncate -s 0 $swapfile
chattr +C $swapfile
btrfs property set $swapfile compression none
fallocate -l 4G $swapfile
chmod 600 $swapfile
mkswap $swapfile
swapon $swapfile

mkdir -p /mnt/boot/efi
mount "$INSTALL_DEVICE""1" /mnt/boot/efi

pacstrap /mnt base base-devel linux linux-firmware btrfs-progs

genfstab -U /mnt > /mnt/etc/fstab

echo "Finished installation step 1"
          exit
          ;;



    step2)

ln -sf /usr/share/zoneinfo/CET /etc/localtime
hwclock --systohc

echo "de_DE.UTF-8 UTF-8" >> /etc/locale.gen
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=de_DE.UTF-8" > /etc/locale.conf
echo "KEYMAP=de-latin1" > /etc/vconsole.conf

# Set the new hostname
echo "Type in the new hostname:"
read new_hostname
echo "$new_hostname" > /etc/hostname

# create keyfile for root decryption (avoid double type password)
dd bs=512 count=4 if=/dev/random of=/crypto_keyfile.bin iflag=fullblock
chmod 600 /crypto_keyfile.bin
chmod 600 /boot/initramfs-linux*
cryptsetup luksAddKey "$INSTALL_DEVICE""2" /crypto_keyfile.bin

# initramfs stuff
install_device_uuid=$(lsblk -dno UUID "$INSTALL_DEVICE""2")
echo "root UUID=$install_device_uuid /crypto_keyfile.bin" >> /etc/crypttab.initramfs

echo "MODULES=()" > /etc/mkinitcpio.conf
echo "BINARIES=(/usr/bin/btrfs)" >> /etc/mkinitcpio.conf
echo "FILES=(/crypto_keyfile.bin)" >> /etc/mkinitcpio.conf
echo "HOOKS=(base systemd autodetect modconf block keyboard sd-vconsole sd-encrypt filesystems fsck)" >> /etc/mkinitcpio.conf
mkinitcpio -P

# set the root password
echo "Set the new root password:"
passwd

# install GRUB - also install microcode updates
pacman --noconfirm -S grub efibootmgr intel-ucode wget bc
echo "GRUB_ENABLE_CRYPTODISK=y" >> /etc/default/grub

wget https://raw.githubusercontent.com/osandov/osandov-linux/49aec6b85d8457fa25b5d8f6c2afb3dd4592401a/scripts/btrfs_map_physical.c
echo "2b20085ab49a50911fa4587b477bbf55861a0bc7d0c6809359bea395ac8cb9fc  btrfs_map_physical.c" | sha256sum -c
echo "Check if hash sum for btrfs_map_physical.c is correct. Continue?"
read
gcc -o btrfs_map_physical btrfs_map_physical.c
offset_raw=$(./btrfs_map_physical /swap/swapfile | awk -F '\t' 'FNR==2{print $9}')
pagesize=$(getconf PAGESIZE)
offset=$(echo "$offset_raw/$pagesize" | bc)
sed -i -e "s/\(GRUB_CMDLINE_LINUX=\".*\)\"/\1 resume=\/dev\/mapper\/root resume_offset=$offset\"/" /etc/default/grub

# Tell systemd logind and hibernat not to check the size of the swapfile since that is broken on btrfs
mkdir -p /etc/systemd/system/systemd-logind.service.d
mkdir -p /etc/systemd/system/systemd-hibernate.service.d
echo "[Service]" >> /etc/systemd/system/systemd-logind.service.d/10-bypass-hibernation-memory-check.conf
echo "Environment=\"SYSTEMD_BYPASS_HYBERNATION_MEMORY_CHECK=1\"" \
  >> /etc/systemd/system/systemd-logind.service.d/10-bypass-hibernation-memory-check.conf
echo "[Service]" >> /etc/systemd/system/systemd-hibernate.service.d/10-bypass-hibernation-memory-check.conf
echo "Environment=\"SYSTEMD_BYPASS_HYBERNATION_MEMORY_CHECK=1\"" \
  >> /etc/systemd/system/systemd-hibernate.service.d/10-bypass-hibernation-memory-check.conf

grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

echo "Finished installation step 2"
          exit
          ;;
  esac
done
