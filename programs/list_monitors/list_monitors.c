
#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>

int main(void)
{
	Display *dsp = NULL;
	Window win;
	XRRScreenResources *res = NULL;
	XRROutputInfo *output_info = NULL;

	dsp = XOpenDisplay(NULL);
	win = XDefaultRootWindow(dsp);
	res = XRRGetScreenResourcesCurrent(dsp, win);

	for (int i = 0; i < res->noutput; i++) {
		output_info = XRRGetOutputInfo (dsp, res, res->outputs[i]);
		if (output_info->connection == RR_Connected)
			printf("%s\n", output_info->name);
		XRRFreeOutputInfo(output_info);
	}

	XRRFreeScreenResources(res);
	XCloseDisplay(dsp);

	return 0;
}
